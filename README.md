# Hestia Convert to openLCA

:warning: This software is still in BETA testing. It may not fully function, and if you encounter any problems or have any questions please log [a new issue](https://gitlab.com/hestia-earth/hestia-convert-to-openlca/-/issues/new).

A library to convert from the Hestia data format (described in the [schema](https://www.hestia.earth/schema) and [glossary](https://www.hestia.earth/glossary)) to the [openLCA JSON-LD format](https://greendelta.github.io/olca-schema/).

## Setup

### Installation

To install this package, use Python3.11, 
1. From the `hestia-convert-to-openlca` folder use `make` to create a virtual environment.
```bash 
make install 
```
3. Create a copy of the file `env-template` and name it `.env` (ensure there are no automatic file extensions). Insert your API token in the appropriate line
4. Download and install [OpenLCA](https://www.openlca.org/download/) 
5. Create a new database, with complete reference data
6. Export the database as a JSON-LD file and unzip into a new folder.
8. Run the program (as below), using the folder above for the --input parameter

### Tests

1. From the `hestia-convert-to-openlca` folder use `make` to run the tests.
```bash
make test
```

## Usage
1. Add the `src` directory to your `PYTHONPATH` environment variable
   - Linux: `export PYTHONPATH=$(pwd)/src` in your shell.
   - Windows PowerShell: `$env:PYTHONPATH+=";$(pwd)/src"`
2. Ensure that the `.env` file is correctly filled (as documented in the Installation section)
```bash
python src/hestia_to_olca/cycle_to_olca.py --cycles cx_dtpwud1iu,abcd --input <path to OpenLCA JSON-LD base directory> --output <path to your output folder>
```

Example
```bash
python src/hestia_to_olca/cycle_to_olca.py --cycles cx_dtpwud1iu --input resources/template_olca_database --output outputs/bananan_converted_olca
```
