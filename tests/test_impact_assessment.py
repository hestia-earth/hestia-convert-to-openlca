import unittest

from hestia_to_olca.impact_assessment import create_olca_process


OLCA_PROCESS = {
    "@type": "Process",
    "@id": "abcd",
    "name": "Banana",
    "description": "-",
    "category": "hestia",
    "version": "00.00.002",
    "lastChange": "2023-09-16T00:00:00.000",
    "processType": "UNIT_PROCESS",
    "isInfrastructureProcess": False,
    "processDocumentation": {
        "isCopyrightProtected": False,
        "creationDate": "2023-09-16T00:00:00.000"
    },
    "lastInternalId": 1,
    "parameters": [],
    "exchanges": [
        {
            "@type": "Exchange",
            "isAvoidedProduct": False,
            "isInput": False,
            "amount": 1,
            "internalId": 1,
            "location": {
                "@type": "Location",
                "@id": "ab6c0400-6660-3ef2-919d-512b21dce9ab"
            },
            "flow": {
                "@id": "821e7195-1135-523e-81b3-49ebb6898ed6",
                "@type": "Flow",
                "name": "Banana, fruit"
            },
            "unit": {
                "@type": "Unit",
                "@id": "20aadc24-a391-41cf-b340-3e4529f44bde"
            },
            "flowProperty": {
                "@type": "FlowProperty",
                "@id": "93a60a56-a3c8-11da-a746-0800200b9a66"
            },
            "isQuantitativeReference": True
        },
        {
            "@type": "Exchange",
            "isAvoidedProduct": False,
            "isInput": False,
            "amount": 3.4,
            "internalId": 2,
            "flow": {
                "@id": "87883a4e-1e3e-4c9d-90c0-f1bea36f8014",
                "@type": "Flow",
                "name": "Ammonia"
            },
            "unit": {
                "@type": "Unit",
                "@id": "20aadc24-a391-41cf-b340-3e4529f44bde"
            },
            "flowProperty": {
                "@type": "FlowProperty",
                "@id": "93a60a56-a3c8-11da-a746-0800200b9a66"
            },
            "isQuantitativeReference": False
        },
    ],
    "socialAspects": [],
    "allocationFactors": []
}

IMPACT_ASSESSMENT_1 = {
  "@context": "https://www-staging.hestia.earth/schema/ImpactAssessment.jsonld",
  "createdAt": "2023-09-14",
  "updatedAt": "2023-09-15",
  "@id": "velzvbrb17in",
  "@type": "ImpactAssessment",
  "name": "Banana",
  "functionalUnitQuantity": 1,
  "country": {
        "@type": "Term",
        "termType": "region",
        "name": "Colombia",
        "@id": "GADM-COL"
    },
  "product": {
      "term": {
          "@type": "Term",
          "termType": "crop",
          "name": "Banana, fruit",
          "units": "kg",
          "@id": "bananaFruit"
        },
        "value": [
            55000
        ],
        "primary": True,
        "@type": "Product",
        "economicValueShare": 100,
        "added": [
            "economicValueShare"
        ],
        "addedVersion": [
            "0.51.0"
        ]
    },
    "emissionsResourceUse": [
        {
            "@type": "Indicator",
            "term": {
                "@type": "Term",
                "@id": "nh3ToAirInputsProduction",
                "name": "NH3, to air, inputs production",
                "termType": "emission",
                "units": "kg NH3"
            },
            "value": 3.4
        }
    ]
}

IMPACT_ASSESSMENT_2_NEEDS_AGGREGATION = {
  "@context": "https://www-staging.hestia.earth/schema/ImpactAssessment.jsonld",
  "createdAt": "2023-09-14",
  "updatedAt": "2023-09-15",
  "@id": "velzvbrb17in",
  "@type": "ImpactAssessment",
  "name": "Banana",
  "functionalUnitQuantity": 1,
  "country": {
        "@type": "Term",
        "termType": "region",
        "name": "Colombia",
        "@id": "GADM-COL"
    },
  "product": {
      "term": {
          "@type": "Term",
          "termType": "crop",
          "name": "Banana, fruit",
          "units": "kg",
          "@id": "bananaFruit"
        },
        "value": [
            55000
        ],
        "primary": True,
        "@type": "Product",
        "economicValueShare": 100,
        "added": [
            "economicValueShare"
        ],
        "addedVersion": [
            "0.51.0"
        ]
    },
    "emissionsResourceUse": [
        {
            "@type": "Indicator",
            "term": {
                "@type": "Term",
                "@id": "nh3ToAirInputsProduction",
                "name": "NH3, to air, inputs production",
                "termType": "emission",
                "units": "kg NH3"
            },
            "value": 1.7
        },
        {
            "@type": "Indicator",
            "term": {
                "@type": "Term",
                "@id": "nh3ToAirInputsProduction",
                "name": "NH3, to air, inputs production",
                "termType": "emission",
                "units": "kg NH3"
            },
            "value": 1.7
        }
    ]
}

SITE = {
    "@id": "abcd",
    "@type": "Site",
    "siteType": "forest"
}

class TestImpactAssessmentToProcess(unittest.TestCase):
    def test_impact_assessment_to_process(self):
        olca_process_uuid = "abcd"
        out = create_olca_process("tests/test_olca_directory", "tests/test_output", olca_process_uuid, IMPACT_ASSESSMENT_1, SITE)
        self.assertEqual(out, OLCA_PROCESS)

    def test_aggregate_inputs_when_converting_impact_assessment_to_process(self):
        olca_process_uuid = "abcd"
        out = create_olca_process("tests/test_olca_directory", "tests/test_output", olca_process_uuid, IMPACT_ASSESSMENT_2_NEEDS_AGGREGATION, SITE)

        self.assertEqual(out, OLCA_PROCESS)
