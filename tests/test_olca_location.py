import unittest

from hestia_to_olca.locations import map_hestia_country_to_olca

class TestLocation(unittest.TestCase):
    def test_location(self):
        loc = map_hestia_country_to_olca("tests/test_olca_directory", "COL")

        self.assertEqual(loc["@id"], "ab6c0400-6660-3ef2-919d-512b21dce9ab")
