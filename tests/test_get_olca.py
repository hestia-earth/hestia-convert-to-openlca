import unittest

from hestia_to_olca.olca_get import get_olca_node

class TestGetOlcaGet(unittest.TestCase):
    def test_get_olca_unitgroup(self):
        unitgroup_json = get_olca_node('tests/test_olca_directory','unit_groups','62a51661-fca1-4d9c-8e01-71f0590dbb30')
        self.assertIsInstance(unitgroup_json, dict)
        self.assertEqual(unitgroup_json['@type'],'UnitGroup')
        self.assertEqual(unitgroup_json['@id'],'62a51661-fca1-4d9c-8e01-71f0590dbb30')
        expected_keys = {'@type', '@id', 'name', 'description', 'category', 'version', 'defaultFlowProperty', 'units'}
        unexpected_keys = set(unitgroup_json.keys()).difference(expected_keys)
        self.assertEqual(len(unexpected_keys), 0)
