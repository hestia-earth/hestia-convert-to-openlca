import unittest
import uuid

from hestia_to_olca.units import map_hestia_unit_to_olca_unit


class TestUnits(unittest.TestCase):
    def test_convert_kg(self):
        unit_id, unit_group_id = map_hestia_unit_to_olca_unit("kg CH4")
        self.assertIsInstance(unit_id, str)
        self.assertIsInstance(unit_group_id, str)
        self.assertEqual(unit_group_id,  '93a60a57-a4c8-11da-a746-0800200c9a66')
        self.assertEqual(unit_id, '20aadc24-a391-41cf-b340-3e4529f44bde')


if __name__ == '__main__':
    unittest.main()
