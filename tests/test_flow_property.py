import unittest

from hestia_to_olca.flow_property_factor import make_flow_property_factor


EXPECTED = {
    "@type": "FlowPropertyFactor",
    "isRefFlowProperty": True,
    "flowProperty": {
        "@type": "FlowProperty",
        "@id": "93a60a56-a3c8-21da-a746-0800200c9a66",
        "name": "Area*time",
        "category": "Technical flow properties",
        "refUnit": "m2*a"
    },
    "conversionFactor": 1.0
}

class TestFlowPropertyFactor(unittest.TestCase):
    def test_make_flow_property_factor(self):
        flow_property_factor = make_flow_property_factor("tests/test_olca_directory", "m2*year")
        self.assertEqual(flow_property_factor, EXPECTED)
