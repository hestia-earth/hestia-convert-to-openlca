import unittest
import json

from hestia_to_olca.product_flow import cycle_products_to_flow


class TestCycleProductToFlow(unittest.TestCase):
    def test_products_to_flows(self):
        with open('tests/banana_cycle_recalculated.json') as f:
            cycle = json.load(f)

        flows = cycle_products_to_flow(cycle, "tests/test_olca_directory")
        self.assertEqual(flows[0]["name"], "Banana, fruit")
        self.assertEqual(
            flows[0]["@id"],
            '821e7195-1135-523e-81b3-49ebb6898ed6',
        )
        # must have at least one waste flow
        self.assertTrue([f for f in flows if f["flowType"] == "WASTE_FLOW"])


if __name__ == '__main__':
    unittest.main()
