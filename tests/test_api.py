import os
import unittest

from hestia_earth.schema import SchemaType

from hestia_to_olca import api


class TestApiCalls(unittest.TestCase):
    def test_get_node_and_related(self):
        if "API_ACCESS_TOKEN" in os.environ:
            node_id = "cx_dtpwud1iu"
            banana = api.get_node(SchemaType.CYCLE, node_id, use_cache=False)
            self.assertEqual(banana["@id"], node_id)

            related_nodes = api.find_related_nodes(SchemaType.CYCLE, node_id, SchemaType.IMPACTASSESSMENT, use_cache=False)
            self.assertGreaterEqual(len(related_nodes), 1)
            self.assertEqual(related_nodes[0]["@type"], "ImpactAssessment")
