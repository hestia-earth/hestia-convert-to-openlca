import unittest
import json

from hestia_to_olca.elementary_flow_mapping import get_hestia_term_id_to_olca_flow_id


class TestCycleProductToFlow(unittest.TestCase):

    def test_map_hestia_to_single_olca_flow(self):
        # Test the case of term_id -> One OpenLCA flow
        hestia_term_id = 'bod5ToWaterIndustrialProcesses'
        olca_flow_id, year_factor = get_hestia_term_id_to_olca_flow_id(hestia_term_id, 'pond')
        self.assertEqual(olca_flow_id, '91955aba-6ebc-4413-9342-f0298860b4aa')
        self.assertEqual(year_factor, 1)

    def test_map_hestia_to_olca_flow_site_type_needed(self):
        # Test the case of term_id -> Multiple OpenLCA flow options, depending on the siteType
        hestia_term_id = 'landOccupationDuringCycle'
        olca_flow_id, year_factor = get_hestia_term_id_to_olca_flow_id(hestia_term_id, 'glass or high accessible cover')
        self.assertEqual(olca_flow_id, '89823c78-b1cc-398f-946e-36a47fdac7cb')
        self.assertEqual(year_factor, 1)

    def test_map_hestia_to_olca_flow_land_transformation_factors(self):
        # Test the year_factor varies for land transformation
        hestia_term_id = "landTransformationFromForest20YearAverageDuringCycle"
        olca_flow_id, year_factor = get_hestia_term_id_to_olca_flow_id(hestia_term_id, 'glass or high accessible cover')
        self.assertEqual(olca_flow_id, "e717f3cc-ac70-4c9b-be56-1614239b917e")
        self.assertEqual(year_factor, 20)

        hestia_term_id = "landTransformationFromPermanentPasture100YearAverageDuringCycle"
        olca_flow_id, year_factor = get_hestia_term_id_to_olca_flow_id(hestia_term_id, 'glass or high accessible cover')
        self.assertEqual(olca_flow_id, "2c126bcc-bb63-4d63-bd72-f02a1e616809")
        self.assertEqual(year_factor, 100)


if __name__ == '__main__':
    unittest.main()

