import unittest

from hestia_to_olca import olca_uuid

INPUT = {
    "term": {
        "@type": "Term",
        "name": "Single Super Phosphate (kg P2O5)",
        "termType": "inorganicFertiliser",
        "@id": "singleSuperPhosphateKgP2O5",
        "units": "kg P2O5",
    },
    "value": [90.00200000000001],
    "@type": "Input",
}

EMISSION = {
    "@type": "Emission",
    "term": {
        "@type": "Term",
        "@id": "nh3ToAirInputsProduction",
        "name": "NH3, to air, inputs production",
        "termType": "emission",
        "units": "kg NH3",
    },
    "methodModel": {
        "@type": "Term",
        "@id": "ecoinventV3",
        "name": "ecoinvent v3",
        "termType": "model",
    },
    "value": [0.01785367747504722],
    "methodTier": "background",
    "inputs": [
        {
            "@type": "Term",
            "name": "Electricity, grid, market mix",
            "termType": "electricity",
            "@id": "electricityGridMarketMix",
            "units": "kWh",
            "added": ["name", "termType", "@id", "units"],
            "addedVersion": ["0.51.0", "0.51.0", "0.51.0", "0.51.0"],
        }
    ],
    "operation": {
        "@type": "Term",
        "termType": "operation",
        "name": "Cooling food, with refrigerator",
        "units": "hour",
        "@id": "coolingFoodWithRefrigerator",
    },
    "added": [
        "term",
        "methodModel",
        "value",
        "methodTier",
        "inputs",
        "operation",
        "transformation",
    ],
    "addedVersion": ["0.51.0", "0.51.0", "0.51.0", "0.51.0", "0.51.0", "0.51.0", "0.51.0"],
}


IMPACT_ASSESSMENT = {
    "@context": "https://www-staging.hestia.earth/schema/ImpactAssessment.jsonld",
    "createdAt": "2023-09-14",
    "updatedAt": "2023-09-15",
    "@id": "velzvbrb17in",
    "@type": "ImpactAssessment",
    "name": "Banana",
}


class TestOpenLCAToUUID(unittest.TestCase):
    def test_deterministic(self):
        self.assertEqual(
            olca_uuid.hestia_to_uuid("Term", "asdf"), olca_uuid.hestia_to_uuid("Term", "asdf")
        )

    def test_namespace(self):
        self.assertNotEqual(
            olca_uuid.hestia_to_uuid("Term", "asdf"), olca_uuid.hestia_to_uuid("Emission", "asdf")
        )

    def test_unknown_namespace(self):
        with self.assertRaises(NotImplementedError):
            olca_uuid.hestia_to_uuid("blah", "asdf")

    def test_from_json_input(self):
        self.assertEqual(
            olca_uuid.hestia_input_to_uuid(INPUT), 'dd0500c7-5a28-5370-8eb9-254c9760af26'
        )

    def test_from_json_emission(self):
        self.assertEqual(
            olca_uuid.hestia_emission_to_uuid(EMISSION), '1ec03246-be41-52ea-94b8-5a5c69a87638'
        )

    def test_from_json_impact_assessment(self):
        self.assertEqual(
            olca_uuid.hestia_impact_assessment_to_uuid(IMPACT_ASSESSMENT),
            'e3d115c1-304b-559f-8c90-4ed1274e73c5',
        )


if __name__ == '__main__':
    unittest.main()
