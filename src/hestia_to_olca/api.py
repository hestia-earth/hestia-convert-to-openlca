import json
import os
import pathlib
import tempfile
from typing import Any, Dict

from dotenv import load_dotenv
from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import download_hestia, find_related


load_dotenv()


def get_node(node_type: SchemaType, node_id: str, use_cache: bool=True) -> Dict[str, Any]:
    """
    Example: `get_node(SchemaType.CYCLE, "cx_dtpwud1iu")`
    """
    if use_cache:
        cached_file_path = os.path.join(tempfile.gettempdir(), "hestia", node_type.name, f"{node_id}.json")
        if os.path.exists(cached_file_path):
            with open(cached_file_path) as cached_file:
                return json.load(cached_file)

    node_dict = download_hestia(node_id, node_type, data_state="recalculated")

    if use_cache:
        cache_dir = os.path.join(tempfile.gettempdir(), "hestia", node_type.name)
        cached_file_path = os.path.join(cache_dir, f"{node_id}.json")
        if not os.path.exists(cached_file_path):
            pathlib.Path(cache_dir).mkdir(parents=True, exist_ok=True)
            with open(cached_file_path, "w") as cache_file:
                json.dump(node_dict, cache_file)

    return node_dict


def find_related_nodes(source_type: SchemaType, source_id: str, target_type: SchemaType, use_cache: bool=True, limit: int=100, offset: int=0):
    """
    Find related nodes to a given source node
    """
    return [
        get_node(target_type, related_node["@id"], use_cache=use_cache)
        for related_node in find_related(source_type, source_id, target_type, limit=limit, offset=offset)
    ]
