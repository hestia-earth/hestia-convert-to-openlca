import uuid


def hestia_input_to_uuid(input):
    "convert deterministically a Hestia Input item to an UUID for OpenLCA."
    assert input["@type"] == "Input"
    return hestia_term_to_uuid(input)


def hestia_emission_to_uuid(input):
    "convert deterministically a Hestia Emission item to an UUID for OpenLCA."
    assert input["@type"] == "Emission"
    return hestia_term_to_uuid(input)


def hestia_term_to_uuid(input):
    "take any hestia item and crate a uuid from it's term-id and the items type."
    if input.get("term"):
        namespace = input["@type"]
        value = input["term"]["@id"]
    return hestia_to_uuid(namespace, value)


def hestia_impact_assessment_to_uuid(input):
    "convert deterministically a Hestia ImpactAssessment item to an UUID for OpenLCA."
    assert input["@type"] == "ImpactAssessment"
    namespace = input["@type"]
    value = input["@id"]
    return hestia_to_uuid(namespace, value)


def hestia_to_uuid(namespace, value):
    """Make a v5 UUID from a namespace and value.

    namespace can be "Term", "Input" or "Emission".
    raises NotImplementedError otherwise
    """
    ns = None
    match namespace:
        case "Term":
            ns = uuid.UUID('649fe36c-67b0-4952-8d51-86e71ff4465b')
        case "Input":
            ns = uuid.UUID('649fe36c-67b0-5a63-8d51-86e71ff4465b')
        case "Emission":
            ns = uuid.UUID('538ed25b-67b0-4952-8d51-97f82005576c')
        case "Product":
            ns = uuid.UUID('427dc14a-67b0-4952-8d51-97f82005576c')
        case "ImpactAssessment":
            ns = uuid.UUID('538ed25b-67b0-4952-8d51-97f82005576d')
        case _:
            raise NotImplementedError

    return str(uuid.uuid5(ns, value))
