from hestia_to_olca import olca_get, units


def make_flow_property_factor(olca_folder: str, hestia_unit: str):
    """
    Generate a flow property factor for a flow given a `hestia_unit` (fetched from the Term) and the `olca_folder`
    """
    resp_or_none = units.map_hestia_unit_to_olca_unit(hestia_unit)
    if resp_or_none is None:
        raise ValueError(f"Could not find Hestia unit: {hestia_unit}")

    olca_unit_id, olca_unit_group_uuid = resp_or_none
    
    unit_group = olca_get.get_olca_node(olca_folder, "unit_groups", olca_unit_group_uuid)
    default_flow_property = unit_group["defaultFlowProperty"]

    matching_olca_units = [
        unit for unit in unit_group["units"]
        if unit["@id"] == olca_unit_id
    ]
    assert len(matching_olca_units) == 1
    olca_unit = matching_olca_units[0]

    return {
        "@type": "FlowPropertyFactor",
        "isRefFlowProperty": True,
        "flowProperty": default_flow_property,
        "conversionFactor": olca_unit["conversionFactor"]
    }
