import logging
from typing import Any, Dict

from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import download_hestia, find_related

from hestia_to_olca import api, hestia_values, olca_make_ref, olca_get, olca_store, olca_uuid, term_flow, units, elementary_flow_mapping, locations

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def create_impact_assessments_from_cycle(input_folder: str, output_folder: str, hestia_cycle_id: str):
    """
    Create all impact assessments from a given cycle
    """
    cycle = api.get_node(SchemaType.CYCLE, hestia_cycle_id)
    site = api.get_node(SchemaType.SITE, cycle["site"]["@id"])
    impact_assessments = api.find_related_nodes(SchemaType.CYCLE, hestia_cycle_id, SchemaType.IMPACTASSESSMENT)
    for impact_assessment in impact_assessments:
        olca_process_uuid = olca_uuid.hestia_impact_assessment_to_uuid(impact_assessment)
        logger.info(f"Hesta impact assessment {impact_assessment['@id']} to Olca process {olca_process_uuid}")
        olca_process = create_olca_process(input_folder, output_folder, olca_process_uuid, impact_assessment, site)
        olca_store.store_olca_node(output_folder, "processes", olca_process_uuid, olca_process)


def create_olca_process(input_folder: str, output_folder: str, olca_process_uuid: str, impact_assessment: Dict[str, Any], site: Dict[str, Any]):
    """
    Create an OlcaProcess from an ImpactAsssement
    """
    dt = "2023-09-16T00:00:00.000"

    output_product = impact_assessment["product"]
    output_product_flow = term_flow.product_to_flow(output_product, input_folder)
    logger.info(f"Hesta product term {output_product['term']['@id']} to Olca output flow {output_product_flow['@id']}")
    olca_store.store_olca_node(output_folder, "flows", output_product_flow["@id"], output_product_flow)

    olca_unit_uuid, _ = units.map_hestia_unit_to_olca_unit(output_product["term"]["units"])

    full_country_code = impact_assessment["country"]["@id"]
    hestia_country_code = full_country_code.split("-")[1]
    location = locations.map_hestia_country_to_olca(input_folder, hestia_country_code)

    output_exchange = {
        "@type": "Exchange",
        "isAvoidedProduct": False,
        "isInput": False,
        "amount": impact_assessment["functionalUnitQuantity"],
        "internalId": 1,
        "flow": olca_make_ref.olca_make_ref_from_json(output_product_flow),
        "unit": {
            "@type": "Unit",
            "@id": olca_unit_uuid
        },
        "flowProperty": {
            "@type": "FlowProperty",
            "@id": output_product_flow["flowProperties"][0]["flowProperty"]["@id"]
        },
        "isQuantitativeReference": True
    }
    if location:
        output_exchange["location"] = {
            "@type": "Location",
            "@id": location["@id"]
        }

    input_exchanges = []

    # Group emissions by id for OpenLCA
    input_exchanges_by_olca_flow_id = dict()

    for n, hestia_emission_or_resource in enumerate(impact_assessment["emissionsResourceUse"]):
        try:
            olca_flow_uuid, year_factor = elementary_flow_mapping.get_hestia_term_id_to_olca_flow_id(hestia_emission_or_resource["term"]["@id"], site["siteType"])
        except BaseException as e:
            logger.warn(f"Issue with {hestia_emission_or_resource['term']['@id']}: {e}. Moving on.")
            continue

        logger.info(f"Hesta input term {hestia_emission_or_resource['term']['@id']} to Olca input flow {olca_flow_uuid}")

        if olca_flow_uuid in input_exchanges_by_olca_flow_id:
            # TODO: Test that updating the reference works
            input_exchange = input_exchanges_by_olca_flow_id.get(olca_flow_uuid)
            incoming_amount = hestia_values.sum_values(hestia_emission_or_resource["value"])
            previous_amount = input_exchange["amount"]
            aggregated_amount = previous_amount + incoming_amount
            input_exchange["amount"] = aggregated_amount * year_factor
        else:
            flow = olca_get.get_olca_node(input_folder, "flows", olca_flow_uuid)
            olca_unit_uuid, _ = units.map_hestia_unit_to_olca_unit(hestia_emission_or_resource["term"]["units"])
            is_input = hestia_emission_or_resource["term"]["termType"] != "emission"
            input_exchange = {
                "@type": "Exchange",
                "isAvoidedProduct": False,
                "isInput": is_input,
                "amount": hestia_values.sum_values(hestia_emission_or_resource["value"]) * year_factor,
                "internalId": n + 2,
                "flow": olca_make_ref.olca_make_ref_from_json(flow),
                "unit": {
                    "@type": "Unit",
                    "@id": olca_unit_uuid
                },
                "flowProperty": {
                    "@type": "FlowProperty",
                    "@id": flow["flowProperties"][0]["flowProperty"]["@id"]
                },
                "isQuantitativeReference": False
            }

            input_exchanges_by_olca_flow_id[olca_flow_uuid] = input_exchange
            input_exchanges = list(input_exchanges_by_olca_flow_id.values())

    return {
        "@type": "Process",
        "@id": str(olca_process_uuid),
        "name": impact_assessment["name"],
        "category": "hestia",
        "description": "-",
        "version": "00.00.002",
        "lastChange": dt,
        "processType": "UNIT_PROCESS",
        "isInfrastructureProcess": False,
        "processDocumentation": {
            "isCopyrightProtected": False,
            "creationDate": dt
        },
        "lastInternalId": 1,
        "parameters": [],
        "exchanges": [output_exchange] + input_exchanges,
        "socialAspects": [],
        "allocationFactors": []
    }
