import logging
from typing import Any, Dict

from hestia_to_olca.olca_get import get_olca_node

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def olca_make_ref_from_json(j: Dict[str, Any]):
    return {
        "@id": j["@id"],
        "@type": j["@type"],
        "name": j.get("name")
    }


def olca_make_ref(input_folder, olca_uuid, olca_type) -> dict:
    '''Make a Ref (reference) object for a given OpenLCA object.
    Note that it will currently only fill the @id, @type, name fields

    TODO: Extend the reference to fill other metadata in line with OpenLCA-generated files
    '''
    olca_json = get_olca_node(input_folder, olca_type, olca_uuid)

    olca_ref = {
        '@id': olca_uuid,
        '@type': olca_type,
        'name': olca_json.get("name"),
    }

    return olca_ref

