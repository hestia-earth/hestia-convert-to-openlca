import uuid
import logging

from hestia_to_olca.lookups.unit_lookups import hestia_unit_to_openlca_unit
from typing import Optional, Tuple

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def map_hestia_unit_to_olca_unit(hestia_unit_name: str) -> Optional[Tuple[str, str]]:
    """Maps a HESTIA unit (a string value, like 'kg CH4') to an openLCA unit
     :Returns
        (unit_id, unit_group_id) if there is a mapping defined in lookups/unit_lookups.py
        None if there is no known mapping for it.
    """
    if hestia_unit_name not in hestia_unit_to_openlca_unit:
        logger.log(logging.WARN, f"Unit {hestia_unit_name} has no known unit, cannot find unit")
        return None

    olca_unit_id  = hestia_unit_to_openlca_unit[hestia_unit_name]["unitId"]
    olca_unit_group_id = hestia_unit_to_openlca_unit[hestia_unit_name]["unitGroupId"]

    # Validate that the unit and unit group id are uuids
    assert uuid.UUID(olca_unit_id)
    assert uuid.UUID(olca_unit_group_id)

    return olca_unit_id, olca_unit_group_id
