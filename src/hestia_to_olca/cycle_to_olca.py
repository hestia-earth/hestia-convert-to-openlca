import argparse
import json
import logging
import os
import pathlib
import shutil
from typing import List

from hestia_to_olca import impact_assessment

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def export_cycle_to_olca(input_folder: str, output_folder: str, cycle_ids: List[str], rm: bool=False):
    """
    Given a Hestia `cycle_id`, fetches all associated data and stores it in OpenLCA format under `output_folder`
    """
    if rm and os.path.exists(output_folder):
        logger.info(f"Deleting {output_folder}")
        shutil.rmtree(output_folder)
    pathlib.Path(output_folder).mkdir(parents=True, exist_ok=True)

    # Default context for import
    with open(os.path.join(output_folder, "openlca.json"), "w") as f:
        json.dump({"schemaVersion": 2}, f)

    # Store the impact assessments
    for idx, cycle_id in enumerate(cycle_ids):
        logger.info(f"[{idx + 1}/{len(cycle_ids)}] Processing Hestia Cycle {cycle_id}")
        impact_assessment.create_impact_assessments_from_cycle(input_folder, output_folder, cycle_id)


def parse_args():
    """
    Returns a configured argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--cycles", required=True, help="Hestia cycle IDs, comma separated")
    parser.add_argument("--input", required=True, help="OpenLCA input folder")
    parser.add_argument("--output", required=True, help="OpenLCA output folder")
    parser.add_argument("--rm", action="store_true", help="If set, then the directory will be deleted")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    export_cycle_to_olca(cycle_ids=args.cycles.split(","), input_folder=args.input, output_folder=args.output, rm=args.rm)
