import glob
import json
import os
from typing import Any, Dict

import pycountry


def map_hestia_country_to_olca(olca_folder: str, hestia_country_code: str, locations_by_code: Dict[str, Dict[str, Any]]=None):
    """
    Map a 3 letter country code to 
    """
    country = pycountry.countries.get(alpha_3=hestia_country_code)
    locations_by_code = load_locations_by_code(olca_folder) if locations_by_code is None else locations_by_code
    country_alpha_2 = locations_by_code.get(country.alpha_2)
    if country_alpha_2 is not None:
        return country_alpha_2
    country_alpha_3 = locations_by_code.get(country.alpha_3)
    if country_alpha_3 is not None:
        return country_alpha_3


def load_locations_by_code(olca_folder: str):
    """
    Load all OpenLCA locations by code
    """
    locations_by_code = {}
    for loc_fp in glob.glob(os.path.join(olca_folder, "locations", "*.json")):
        with open(loc_fp) as loc_f:
            loc_json = json.load(loc_f)
        locations_by_code[loc_json["code"]] = loc_json
    return locations_by_code
