import json
import logging
import os
import pathlib
import uuid

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def get_olca_node(input_folder: str, olca_type: str, olca_uuid: str) -> None | dict:
    """Get an entire OpenLCA node as a json object from a input_folder
    Will raise an error in the logger if the node is not found"""

    assert isinstance(input_folder, str)
    assert isinstance(olca_type, str)
    assert uuid.UUID(olca_uuid)

    file_dir = pathlib.Path(input_folder) / olca_type
    fp = file_dir / f"{olca_uuid}.json"
    if not os.path.exists(fp):
        logger.error(f'{fp} not found')
        return

    with open(fp) as f:
        return json.load(f)
