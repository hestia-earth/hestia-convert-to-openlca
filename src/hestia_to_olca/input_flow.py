from typing import Any, Dict

from hestia_to_olca import term_flow


def cycle_inputs_to_flow(cycle: Dict[str, Any], olca_folder: str) -> Dict[str, Any]:
    assert cycle["@type"] == "Cycle"
    return [term_flow.product_to_flow(p, olca_folder) for p in cycle["inputs"]]
