import logging
import uuid

from hestia_to_olca.lookups.elementary_flow_lookups import (
    hestia_term_id_to_openlca_flow,
    land_transformation_20_year_lookups,
    land_transformation_100_year_lookups
)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

hestia_sitetypes = {'forest', 'other natural vegetation', 'cropland', 'glass or high accessible cover',
                    ' permanent pasture', 'animal housing', 'pond', 'river or stream', 'lake', 'sea or ocean',
                    ' agri-food processor', 'food retailer'}


def get_hestia_term_id_to_olca_flow_id(hestia_term_id: str, site_type: str) -> tuple[str, int]:
    """
    Given a Hestia term id (a string), lookup the OpenLCA flow id in substance_lookup.py
    Also returns a year factor, defaulting to 1 to compensate for the division of land transformation values by the
    years considered in the hestia resource
    e.g. here https://www.hestia.earth/term/landTransformationFromPermanentCropland20YearAverageInputsProduction”
    """
    olca_flow_lookup_value = hestia_term_id_to_openlca_flow.get(hestia_term_id)
    year_factor = 1
    if olca_flow_lookup_value is None:
        if hestia_term_id in land_transformation_20_year_lookups:
            olca_flow_lookup_value = land_transformation_20_year_lookups[hestia_term_id]
            year_factor = 20
        elif hestia_term_id in land_transformation_100_year_lookups:
            olca_flow_lookup_value = land_transformation_100_year_lookups[hestia_term_id]
            year_factor = 100

    if site_type not in hestia_sitetypes:
        logger.error(f'"{site_type}" is not a recognized Hestia site type. Has the Hestia schema changed?')
        raise ValueError(f'"{site_type}" is not a recognized Hestia site type. Has the Hestia schema changed?')
    if isinstance(olca_flow_lookup_value, list):
        for i, term_mapping in enumerate(olca_flow_lookup_value):
            if 'siteType' not in term_mapping:
                logger.error(f'Issue found in mapping. Multiple mappings available for hestia term id {hestia_term_id}, but no siteType set on array index {i}')
                raise ValueError(f'Issue found in mapping. Multiple mappings available for hestia term id {hestia_term_id}, but no siteType set on array index {i}')
            if site_type != term_mapping['siteType']:
                continue
            olca_flow_id = term_mapping['uuid']
            assert uuid.UUID(olca_flow_id)
            return olca_flow_id, year_factor
    elif isinstance(olca_flow_lookup_value, dict):
        olca_flow_id = olca_flow_lookup_value['uuid']
        assert uuid.UUID(olca_flow_id)
        return olca_flow_id, year_factor
    else:
        logger.error(f'Issue found in mapping for {hestia_term_id}. Expecting a dictionary or list, received {type(olca_flow_lookup_value)}')
        raise Exception(f'Issue found in mapping for {hestia_term_id}.')

