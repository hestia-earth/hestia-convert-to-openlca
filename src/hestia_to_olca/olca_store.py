import json
import os
import pathlib
from typing import Any, Dict


def store_olca_node(output_folder: str, olca_type: str, olca_uuid: str, node_data: Dict[str, Any], overwrite: bool=False):
    """
    Store an Olca JSON Node into the right file, from the `output_folder`
    If the file exists and `overwrite=False`, return early
    """
    file_dir = os.path.join(output_folder, olca_type)
    fp = os.path.join(file_dir, f"{str(olca_uuid)}.json")
    if os.path.exists(fp) and not overwrite:
        return
    pathlib.Path(file_dir).mkdir(parents=True, exist_ok=True)

    with open(fp, "w") as f:
        json.dump(node_data, f)
