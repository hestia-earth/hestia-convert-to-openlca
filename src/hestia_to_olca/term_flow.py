import copy
from datetime import datetime
from typing import Any, Dict

from hestia_earth.schema import SchemaType

from hestia_to_olca import api, flow_property_factor, olca_uuid

FLOW_TEMPLATE = {
    "@type": "Flow",
    "@id": "3f376415-fc7f-485b-9713-609008c9994b",
    "name": "banana",
    "description": "EcoSpold 2 intermediate exchange, ID \u003d 3f376415-fc7f-485b-9713-609008c9994b",
    "category": "A:Agriculture, forestry and fishing/01:Crop and animal production, hunting and related service activities/012:Growing of perennial crops/0122:Growing of tropical and subtropical fruits",
    "version": "00.00.003",
    "lastChange": "2021-11-11T18:55:18.639Z",
    "flowType": "PRODUCT_FLOW",
    "isInfrastructureFlow": False,
    "flowProperties": [
        {
            "@type": "FlowPropertyFactor",
            "isRefFlowProperty": True,
            "flowProperty": {
                "@type": "FlowProperty",
                "@id": "93a60a56-a3c8-11da-a746-0800200b9a66",
                "name": "Mass",
                "category": "Technical flow properties",
                "refUnit": "kg",
            },
            "conversionFactor": 1.0,
        }
    ],
}


def now_iso8601():
    current_date = datetime.now()
    return current_date.strftime("%Y-%m-%dT%H:%M:%SZ")


def product_to_flow(product: Dict[str, Any], olca_folder: str) -> Dict[str, Any]:
    flow = copy.deepcopy(FLOW_TEMPLATE)
    # fetch term from reference
    fpf = flow_property_factor.make_flow_property_factor(olca_folder, product["term"]["units"])
    term = api.get_node(SchemaType.TERM, product["term"]["@id"])
    flow["@id"] = olca_uuid.hestia_term_to_uuid(product)
    flow["name"] = term["name"]
    flow["@context"] = term["@context"]
    flow["flowProperties"] = [fpf]
    if term.get("updatedAt"):
        flow["lastChange"] = f'{term["updatedAt"]}T00:00:00Z'
    elif term.get("createdAt"):
        flow["lastChange"] = f'{term["createdAt"]}T00:00:00Z'
    else:
        flow["lastChange"] = now_iso8601()
    flow["description"] = term.get("definition", "")
    if term.get("casNumber"):
        flow["cas"] = term["casNumber"]
    if term["termType"] in ("waste", "cropResidue"):
        flow["flowType"] = "WASTE_FLOW"
    else:
        flow["flowType"] = "PRODUCT_FLOW"
    flow["category"] = "hestia"
    return flow
