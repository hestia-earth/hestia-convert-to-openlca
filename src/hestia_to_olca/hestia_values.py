def sum_values(values: list | float | int) -> float:
    """
    Sum a list or single value to get a single float
    """
    if isinstance(values, list):
        return sum(values)
    elif isinstance(values, float) or isinstance(values, int):
        return float(values)
    else:
        raise ValueError("Unsupported type in sum_values {values}")